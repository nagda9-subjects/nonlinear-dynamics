%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           Practice          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Nonlinear dynamics systems  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         2020.11.19.         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           Fractals          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Fern fractal

% Parameters:
x = [.5; .5];

darkgreen = [0 2/3 0];

p=[ 0.97 0.98 0.99]; % 0.25 0.50 0.75 -> the middle 0.97 0.98 0.99 -> the side is better visible
A1=[ .85 .04; -.04 .85];
A2=[ .20 -.26; .23 .22];
A3=[-.15 .28; .26 .24];
A4=[0 0 ; 0 .16];
b1 = [0; 1.6];
b2 = [0; 1.6];
b3 = [0; .44];

figure;
iternum=1000000;
X=zeros(iternum, 1);
Y=zeros(iternum, 1);

xlim([-3, 3]);
ylim([0 10]);
title('Fractal fern');

hold on;
for sz=1:iternum %apply the 4 rules in random order
r = rand;
if r < p(1)
x = A1*x + b1;
elseif r < p(2)
x = A2*x + b2;
elseif r < p(3)
x = A3*x + b3;
else
x = A4*x;
end
% step by step plotting:
%plot(x(1), x(2), '.', 'MarkerSize', 5, 'color', darkgreen);
%xlabel(['Iteration: ', num2str(sz)]);
%drawnow
X(sz,1)=x(1);
Y(sz,1)=x(2);
end
plot(X, Y, '.', 'MarkerSize', 2, 'color', darkgreen);
xlabel(['Iteration: ', num2str(iternum)]);
%hold off;

