%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           Practice          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Nonlinear dynamics systems  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         2020.11.19.         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           Fractals          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cantor set

figure;
title('Cantor set');
%pontok=linspace(0,1, 10); %maps the [0,1] line
pontok=rand %starts from 1 point
length(pontok)
plot(pontok, 0.1*ones(1, length(pontok)), 'b.');
xlabel('iteration: 1');
xlim([0,1]);
ylim([0,2]);

hold on;

for i=2:20
    waitforbuttonpress;  
    pontok=[1/3*pontok, 1/3*pontok+2/3]; %map the previous lines to their 1. and 3. third
    plot(pontok, 0.1*i*ones(1, length(pontok)), 'b.');
    xlabel(['iteration: ', num2str(i)]);
    xlim([0,1]);
    ylim([0,2.1]);
end