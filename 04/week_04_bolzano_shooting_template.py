import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from scipy.integrate import solve_ivp

######## Exercises: ########
# (1) Add damping by changing parameter `b`
#     to some strictly positive value.
#     Try shooting to (pi, 0) again.
# (2) Zoom in to the most interesting region of
#     the plot -- how can you do that easily?
# (3) Automate the bisection -- detect which
#     region needs zooming (how?), and loop
#     by recomputing initial values, until (pi, 0)
#     is approached within a predefined accuracy.
# (4) Try optimizing the computation of longer
#     and longer trajectories by reusing the
#     already-computed time steps (i.e. don't
#     start again from 0 if the trajectory is
#     not long enough).

b = 0.0               # Damping parameter
x0 = 2.5              # Initial condition x(t0)
dx_a, dx_b = 0.5, 0.7 # Range of initial conditions x'(t0)
num_inits = 11        # Number of initial conditions (== len(range))
tf = 2.0              # Evaluation time length

#################

def y_prime(t, y, b):
    return y[1], -(b * y[1] + np.sin(y[0]))

blue = sns.color_palette()[0] # Seaborn default is nicer than 'blue'

# Draw trajectories with various initial conditions
for dx in np.linspace(dx_a, dx_b, num=num_inits):
    y0 = [x0, dx]
    repeat = True

    while repeat:
        res = solve_ivp(
            y_prime,
            t_span=(0, tf),
            y0=y0,
            t_eval=np.arange(0, tf, 0.01),
            args=(b,),
        )
        t, y = res.t, res.y

        # Gap 1: How to decide if trajectory returns (regular
        # pendulum motion) or enters a full circular motion
        # (always rotates in the same direction)?
        
        if ???:
            color = blue
            repeat = False
        elif ???:
            color = 'orange'
            repeat = False
        else:
            # Gap 2:
            # Neither derivative changed sign, so
            # we must compute a longer trajectory.
            # How to do that?
            pass

    sns.lineplot(y[0], y[1], sort=False, color=color, linewidth=0.75)

    precision = int(
        -np.floor(np.log10(dx_b - dx_a)) \
        + \
        np.round(np.log10(num_inits)) + 1
    )

    # Plot initial x'(t) at end of each trajectory
    plt.text(
        y[0, -1], y[1, -1],
        '{0:.{1}f}'.format(dx, precision),
        horizontalalignment='center',
        verticalalignment='center',
    )

# Draw vertical line at pi for reference
sns.lineplot(
    [np.pi, np.pi+1e-8],
    np.array(plt.gca().get_ylim()) * 0.9,
    color='red',
    label='$\\pi$',
)

plt.legend()
plt.title(
    'Bolzano Shooting of the Damped Pendulum\nFor various values of $x\'(t_0)$'
)
plt.gca().set_xlabel('$x = x(t)$')
plt.gca().set_ylabel('$y = \\frac{dx}{dt}$')
plt.tight_layout()
plt.savefig('bolzano_shooting.pdf')
