import numpy as np
import matplotlib as mpl; mpl.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

from scipy.integrate import solve_ivp
from matplotlib.animation import FuncAnimation

def y_prime(t, y, A):
    # Gap 1: y_dot = A(mu) * y
    # return ...

# Plot phase portrait
fig, ax = plt.subplots()

def plot_phase_portrait(mu):
    print('Solving with mu =', mu)

    # GAP 2: Clear Screen
    # ...

    # GAP 3: Define A(mu)
    # A = ...

    solution = solve_ivp(
        y_prime,
        t_span=(0, 50),
        y0=[1, 1],
        t_eval=np.arange(0, 50, 0.025),
        args=(A,),
    )
    t, y = solution.t, solution.y

    # GAP 4: Compute bounds for meshgrid
    # y0min, y1min = ...
    # y0max, y1max = ...

    # GAP 5: Compute Force Field
    X, Y = np.meshgrid(
        np.linspace(y0min, y0max, num=24),
        np.linspace(y1min, y1max, num=24),
    )
    # XY = ...
    # U, V = ...

    # Normalize Force Field
    mag = np.linalg.norm([U, V], axis=0)
    U, V = U / mag, V / mag

    sns.lineplot(y[0, :], y[1, :], sort=False, ax=ax, color='red')
    plt.quiver(X, Y, U, V, mag, cmap='gnuplot2')
    plt.gca().set_xlabel('y[0]')
    plt.gca().set_ylabel('y[1]')
    plt.title('Phase Portrait at mu = {:.4f}'.format(mu))
    plt.tight_layout()

# GAP 6: Create Animation
# anim = FuncAnimation(
#     ...
# )
# anim.save('phase_portrait.mp4') # write animation to video file
