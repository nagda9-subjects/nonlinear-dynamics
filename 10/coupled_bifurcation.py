#!/usr/bin/env python3

import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import seaborn as sns

def y_prime(t, xy, k):
    x, y = xy

    return [
        x - x**3 + k * (y - x),
        y - y**3 + k * (x - y),
    ]

k = 0.4
ts = (0, 10)

t_eval = np.linspace(ts[0], ts[1], num=101)
x0s, y0s = np.meshgrid(np.linspace(-1, 1, num=13), np.linspace(-1, 1, num=13))
xy0s = np.array([x0s, y0s]).T.reshape(-1, 2)

sns.set()

X, Y = np.meshgrid(np.linspace(-1, 1, num=25), np.linspace(-1, 1, num=25))

for i, k in enumerate(np.linspace(0, 1.0, num=17)):
    print('k =', k)

    print('    drawing phase portrait')
    for xy0 in xy0s:
        res = solve_ivp(y_prime, t_span=ts, y0=xy0, t_eval=t_eval, args=(k,), vectorized=True)
        xys = res.y
        xs, ys = xys[0, :], xys[1, :]

        color = '#{:02x}00{:02x}'.format(int((xy0[0] + 1) * 127.5), int((xy0[1] + 1) * 127.5))
        sns.lineplot(xs, ys, sort=False, color=color, linewidth=1)
    
    plt.title('Coupling and Bifurcation; k = {:.4f}'.format(k))
    plt.tight_layout()
    plt.savefig('coupled_bifurcation_phase_{:03}.png'.format(i), dpi=360)
    plt.close()

    print('    drawing force field')
    U, V = X - X**3 + k * (Y - X), Y - Y**3 + k * (X - Y)
    mag = np.linalg.norm([U, V], axis=0)
    mag[mag == 0] = 1e-9
    U, V = U / mag, V / mag
    
    plt.quiver(X, Y, U, V, mag, cmap='gnuplot2')
    plt.title('Coupling and Bifurcation; k = {:.4f}'.format(k))
    plt.tight_layout()
    plt.savefig('coupled_bifurcation_force_{:03}.png'.format(i), dpi=360)
    plt.close()

    print()
