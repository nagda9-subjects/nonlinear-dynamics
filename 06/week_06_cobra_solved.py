#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from scipy.integrate import solve_ivp
from scipy.optimize import root

sns.set()

# Cobra: A Bifurcation
# 
# * Hopf bifurcation around `mu = 1`: note the attraction vs. repulsion around `(-1, 0)` and `(1, 0)` while crossing the critical value of the parameter
# * Another bifurcation near `mu = 4` and `(x, y) = (0, 0.5`): Saddle-Node!

# Exercise: write the Python function for the following system of DEs:
# 
# ```
# dx/dt = -xy
# dy/dt = x^2 - y - 1 + mu(y - y^3)
# ```

def y_prime(t, xy, mu):
    x, y = xy
    
    return [
        -x * y,
        x**2 - y - 1 + mu * (y - y**3),
    ]


# Exercise: Try adjusting the values of `mu`, for example as specified below:

# mu = 0.75
# mu = 1.25
# mu = 3.8
mu = 4.2

xlim, ylim = (-1.25, 1.25), (-1.5, 1.0)

X, Y = np.meshgrid(np.linspace(*xlim, num=12), np.linspace(*ylim, num=12))
XY = np.array([X, Y]).T.reshape(-1, 2)

plt.figure(figsize=(16, 12))
sns.scatterplot(XY[:, 0], XY[:, 1])

for xy0 in XY:
    res = solve_ivp(y_prime, y0=xy0, t_span=(0, 8), t_eval=np.linspace(0, 8, num=200), args=(mu,))
    t, xy = res.t, res.y
    sns.lineplot(xy[0, :], xy[1, :], sort=False)

plt.savefig('cobra_trajectory_mu_{}.pdf'.format(mu))

# Make a finer grid for visualizing the gradient
X, Y = np.meshgrid(np.linspace(*xlim, num=32), np.linspace(*ylim, num=32))
U, V = -X * Y, X**2 - Y - 1 + mu * (Y - Y**3)
mag = np.sqrt(U**2 + V**2)
U, V = U / mag, V / mag

plt.close()
plt.figure(figsize=(16, 12))
plt.quiver(X, Y, U, V, mag, cmap='gnuplot2')
plt.savefig('cobra_gradient_mu_{}.pdf'.format(mu))
