import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from scipy.integrate import solve_ivp

# Parameters
Bx =  4.0   # Natural birth rate of X
Dx =  1.0   # Natural death rate of X
Ex = -2.0   # Effect of Y on X
By =  6.0   # Natural birth rate of Y
Dy =  1.0   # Natural death rate of Y
Ey = -2.0   # Effect of X on Y

# Initial Values `(x_0, y_0)` for special trajectories
XY0 = [
    (1, 1),
    (7, 7),
    (3, 7),
    (7, 4),
    (7, 2),
    (1, 0.1),
]

# Simulation and Plotting Parameters
tf = 10     # Integration Time
Xmax = 7    # Plot X values from 0 to Xmax
Ymax = 7    # Plot Y values from 0 to Ymax

################# Another possible model:

Bx =  5.0
Dx =  1.0
Ex = -2.0
By =  5.0
Dy =  1.0
Ey =  1.0

tf = 10
Xmax = 10
Ymax = 10

################# A third one:

Bx =  1.0
Dx =  0.5
Ex = -1.0
By =  0.5
Dy =  1.0
Ey = -1.0

tf = 10
Xmax = 8
Ymax = 8

#################

def y_prime(t, ys):
    x, y = ys

    return [
        x * (Bx - Dx * x + Ex * y),
        y * (By - Dy * y + Ey * x),
    ]

blue = sns.color_palette()[0] # Seaborn default is nicer than 'blue'

# Plot normalized force field
X, Y = np.meshgrid(
    np.linspace(0, Xmax, num=24),
    np.linspace(0, Ymax, num=24),
)
U = X * (Bx - Dx * X + Ex * Y)
V = Y * (By - Dy * Y + Ey * X)
mag = np.sqrt(U**2 + V**2)
U, V = U / mag, V / mag
plt.quiver(X, Y, U, V, mag, cmap='gnuplot2')

# Plot DEs for various initial values
for x0, y0 in XY0:
    res = solve_ivp(
        y_prime,
        t_span=(0, tf),
        y0=[x0, y0],
        t_eval=np.arange(0, tf, 0.025),
    )
    t, y = res.t, res.y
    sns.lineplot(
        y[0], y[1],
        sort=False, linewidth=1.2,
        label='$x_0 = {}; y_0 = {}$'.format(x0, y0),
    )

plt.title('Lotka-Volterra Model')
plt.gca().set_xlabel('Species X')
plt.gca().set_ylabel('Species Y')
plt.tight_layout()
plt.savefig('lotka_volterra.pdf')
