import numpy as np
import matplotlib as mpl; #mpl.use('agg') # PDF-be mentes
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import io
import imageio

from scipy.integrate import solve_ivp
from PIL import Image, ImageDraw

images = []

def y_prime(t, y, A):
    # Gap 1: y_dot = A(mu) * y
    # return ...
    return np.matmul(A, y)
    # A @ y

# Plot phase portrait
fig, ax = plt.subplots()

def plot_phase_portrait(mu):
    print('Solving with mu =', mu)

    # GAP 2: Clear Screen
    # ...
    ax.clear()
    
    # GAP 3: Define A(mu)
    A = np.array([[-mu, -1], [9, -2]])
    
    solution = solve_ivp(
        y_prime,
        t_span=(0, 50),
        y0=[1, 1],
        t_eval=np.arange(0, 50, 0.025),
        args=(A,),
    )
    t, y = solution.t, solution.y

    # GAP 4: Compute bounds for meshgrid
    y0min, y1min = np.min(y, axis=1)
    y0max, y1max = np.max(y, axis=1)

    # GAP 5: Compute Force Field
    X, Y = np.meshgrid(
        np.linspace(y0min, y0max, num=24),
        np.linspace(y1min, y1max, num=24),
    )
    XY = np.array([X, Y]).reshape(2, -1).T    #2 sora, annyi oszlop, amennyi kell
    # a reshape a legutolso dimenziotol alakit at
    # numpy soron iteral, ezert transzponaljuk
    U, V = np.array([A @ xy for xy in XY]).T
    # ... for x in X a sorokon megy vegig
    
    # Normalize Force Field
    mag = np.linalg.norm([U, V], axis=0)
    U, V = U / mag, V / mag

    sns.lineplot(y[0, :], y[1, :], sort=False, ax=ax, color='red')
    plt.quiver(X, Y, U, V, mag, cmap='gnuplot2')
    plt.gca().set_xlabel('y[0]')
    plt.gca().set_ylabel('y[1]')
    plt.title('Phase Portrait at mu = {:.1f}'.format(round(mu,1)))
    plt.tight_layout()
    
#plot_phase_portrait(mu=9)
#plt.savefig('phase_portrait_mu_-3.pdf') 
#plt.show()   

# GAP 6: Create Animation
for i in np.linspace(-4.8, 8.2, num=131): #131
    buf = io.BytesIO()
    plot_phase_portrait(mu=i)
    plt.savefig(buf, format='png')
    buf.seek(0)
    im = Image.open(buf)
    images.append(np.array(im))
    buf.close()

imageio.mimsave('movie2.gif', images, format='GIF', duration=0.5)
