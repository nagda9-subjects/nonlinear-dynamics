import numpy as np
import matplotlib as mpl; mpl.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

from scipy.integrate import solve_ivp
from matplotlib.animation import FuncAnimation

def y_prime(t, y, A):
    return A @ y

# Plot phase portrait
fig, ax = plt.subplots()

def plot_phase_portrait(mu):
    print('Solving with mu =', mu)
    ax.clear()

    A = np.array([[-mu, -1],
                  [  9, -2]])

    solution = solve_ivp(
        y_prime,
        t_span=(0, 50),
        y0=[1, 1],
        t_eval=np.arange(0, 50, 0.025),
        args=(A,),
    )
    t, y = solution.t, solution.y

    y0min, y1min = np.min(y, axis=1)
    y0max, y1max = np.max(y, axis=1)

    # Force Field
    X, Y = np.meshgrid(
        np.linspace(y0min, y0max, num=24),
        np.linspace(y1min, y1max, num=24),
    )
    XY = np.array([X, Y]).reshape(2, -1).T
    U, V = np.array([A @ xy for xy in XY]).T
    mag = np.linalg.norm([U, V], axis=0)
    U, V = U / mag, V / mag

    sns.lineplot(y[0, :], y[1, :], sort=False, ax=ax, color='red')
    plt.quiver(X, Y, U, V, mag, cmap='gnuplot2')
    plt.gca().set_xlabel('y[0]')
    plt.gca().set_ylabel('y[1]')
    plt.title('Phase Portrait at mu = {:.4f}'.format(mu))
    plt.tight_layout()

anim = FuncAnimation(
    fig,
    plot_phase_portrait,
    frames=np.linspace(-4, 2, num=601), # values of the parameter `mu`
    interval=333, # 333 ms ~ 3 FPS
)
anim.save('phase_portrait.mp4') # write animation to video file
