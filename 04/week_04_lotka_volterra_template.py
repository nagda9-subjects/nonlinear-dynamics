import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from scipy.integrate import solve_ivp

######## Exercise: ########
# Change the 6 parameters and the set of initial
# values and try to come up with systems that
# behave qualitatively differently (as per the
# global X-Y phase portrait).
#
# You might want to change the X, Y, and tf (time)
# limits along with the parameters, for prettier plots.

# Parameters
Bx =  4.0   # Natural birth rate of X
Dx =  1.0   # Natural death rate of X
Ex = -2.0   # Effect of Y on X
By =  6.0   # Natural birth rate of Y
Dy =  1.0   # Natural death rate of Y
Ey = -2.0   # Effect of X on Y

# Initial Values `(x_0, y_0)` for special trajectories
XY0 = [
    (1, 1),
    (7, 7),
    (3, 7),
    (7, 4),
    (7, 2),
    (1, 0.1),
]

# Simulation and Plotting Parameters
tf = 10     # Integration Time
Xmax = 7    # Plot X values from 0 to Xmax
Ymax = 7    # Plot Y values from 0 to Ymax

#################

def y_prime(t, ys):
    x, y = ys

    # Gap 1: Compute derivative
    return [
        ???,
        ???,
    ]

blue = sns.color_palette()[0] # Seaborn default is nicer than 'blue'

# Gap 2
# Plot normalized force field
X, Y = ???
U, V = ???, ???
plt.quiver(???, ???, ???, ???, ???, cmap='gnuplot2')

# Plot DEs for various initial values
for x0, y0 in XY0:
    res = solve_ivp(
        y_prime,
        t_span=(0, tf),
        y0=[x0, y0],
        t_eval=np.arange(0, tf, 0.025),
    )
    t, y = res.t, res.y

    # Gap 3: plot trajectory
    # Note that LaTeX can be used in text labels, titles, etc.
    sns.lineplot(
        ???
    )

plt.title('Lotka-Volterra Model')
plt.gca().set_xlabel('Species X')
plt.gca().set_ylabel('Species Y')
plt.tight_layout()
plt.savefig('lotka_volterra.pdf')
