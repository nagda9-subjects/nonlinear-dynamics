# This is a complete, self-contained, runnable Python script that demonstrates
# the basics of creating a GUI application in Python, using the popular GTK toolkit
# (and its Python bindings, PyGTK).

# Note that you will need the `pygobject` Python package and
# its dependencies (GObject, GTK+3, etc.) to be installed on your computer.
# You can install `pygobject` by opening a command line and typing:
# `pip3 install pygobject`

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import gi

# Need this line for being able to write `import Gtk`
gi.require_version('Gtk', '3.0')

# GTK3 is a popular GUI package, and Matplotlib supports displaying graphs
# in a GTK window with the help of the `FigureCanvasGTK3Agg` type.
from scipy.integrate import solve_ivp
from gi.repository import Gtk
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas

############### Matplotlib plotting code

def plot_phase_portrait(ax, y_prime, y0, t_eval, args):
    '''
    Computes the solution of `[x', y'] = y_prime(t, [x, y], *args)`
    and plots the resulting phase portrait inside the Axis object `ax`.
    '''
    res = solve_ivp(
        y_prime,
        y0=y0,
        t_span=(t_eval[0], t_eval[-1]),
        t_eval=t_eval,
        args=args,
    )
    t, (x, y) = res.t, res.y

    ax.clear()
    sns.lineplot(x, y, sort=False, ax=ax, label='Phase Space')

    ax.legend()
    ax.set_title('Phase Portrait')
    ax.set_xlabel('Position')
    ax.set_ylabel('Momentum')

###############

def y_prime(t, xy, b):
    '''
    RHS of the differential equation for the damped pendulum
    with damping factor `b`.
    '''
    x, y = xy

    return [
        y,
        -(np.sin(x) + b * y),
    ]

#######################################
# !!! GUI (GTK3) code starts here !!! #
#######################################

def redraw(_widget, canvas, ax, params):
    '''
    This function will be called when the user moves the
    marker on any of the 3 sliders, changing its value.
    
    It grabs the current values of the initial conditions
    as well as the damping parameter, then re-computes the
    solution, and redraws the trajectory on the `canvas`.

    (`_widget` is an ignored parameter; it will simply
    be the widget that caused the re-drawing -- in
    our case, a scale -- because its value changed.
    `None` when the plot is drawn for the first time.)
    '''

    x0 = params['x0']['scale'].get_value()
    y0 = params['y0']['scale'].get_value()
    b  = params['b']['scale'].get_value()
    t_eval = np.linspace(0, 8 * np.pi, num=250)

    # Redraw backing buffer / Axes in Matplotlib
    plot_phase_portrait(ax, y_prime, y0=[x0, y0], t_eval=t_eval, args=(b,))
    # Refresh actual, on-screen contents from Matplotlib buffer
    canvas.draw()

# Grab both the Figure and the Axes objects, because:
# - We will need the Figure for wrapping it in a
#   FigureCanvas so we can display it in a GTK window;
# - But we draw to the Axes, not (directly) to the Figure.
fig, ax = plt.subplots()
canvas = FigureCanvas(fig)
canvas.set_size_request(540, 480)

# `VBox` is a vertical stack of sub-widgets.
# In our case, it will occupy the right half of the screen,
# and it will contain the labels and the sliders (scales)
# below one another.
scale_box = Gtk.VBox()

# Define a bunch of label-slider pairs:
# the label will come first, above the scale,
# and then the next pair below them, etc.
# This `params` dictionary will hold the title
# texts, ranges, and initial values, and in the
# loop below, it will be updated so it contains
# the Scale (slider) objects as well. This is
# needed because the `redraw()` callback will
# use this same dictionary to access the
# current value of each scale object.
params = {
    'x0': {
        'range': (-1, 1),
        'value': 1,
        'label': 'Initial Position',
    },
    'y0': {
        'range': (-1, 1),
        'value': 0,
        'label': 'Initial Momentum',
    },
    'b': {
        'range': (0, 1),
        'value': 0.3,
        'label': 'Damping',
    }
}

for obj in params.values():
    min, max = obj['range']
    value = obj['value']
    title = obj['label']

    # Create a label with the description of the slider.
    # `pack_start()` is the function that adds subwidgets
    # to a `VBox` or `HBox` container.
    label = Gtk.Label(title)
    scale_box.pack_start(label, True, True, 0)

    # Then, create the actual slider as well.
    # First, set its range and initial value.
    # Then, set its `value_changed` callback,
    # so that every time a parameter changes,
    # the trajectory is redrawn accordingly.
    scale = Gtk.HScale()
    scale.set_range(min, max)
    scale.set_value(value)
    scale.connect('value_changed', redraw, canvas, ax, params)
    scale_box.pack_start(scale, True, True, 0)

    obj['scale'] = scale

# `HBox` is a horizontal stack of sub-widgets.
# In our case, the main box contains the drawing
# canvas on the left, and the sliders on the right.
main_box = Gtk.HBox()
main_box.pack_start(canvas, True, True, 0)
main_box.pack_start(scale_box, True, True, 0)

# Create a window, which is the main thing that
# shows up, and contains every other GUI widget.
# Set its size and its main title, and make sure
# that the program exits when the 'X' or 'Close'
# button of the top toolbar is clicked.
win = Gtk.Window()
win.connect('delete-event', Gtk.main_quit)
win.set_title('Damped Pendulum')
win.set_default_size(960, 480)
win.add(main_box) # Set the main box as the contents of the window
win.show_all() # Display the window with all its contents

# Plot phase space for the first time, with the default parameters.
redraw(None, canvas, ax, params)

# Once everything is set up, start the GUI event loop
Gtk.main()
