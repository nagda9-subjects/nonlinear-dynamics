#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.integrate import solve_ivp
from mpl_toolkits.mplot3d import Axes3D


def g(x, g1, g3):
    # return g1 * x + g3 * x**3
    return g3 * x + 0.5 * (g1 - g3) * (np.abs(x + 1) - np.abs(x - 1))

def y_prime_simple(t, xyz, a, b, g1, g3):
    x, y, z = xyz

    dx = a * (y - x - g(x, g1, g3))
    dy = x - y + z
    dz = -b * y

    return [dx, dy, dz]

def y_prime_coupled(t, xyzxyz, a, b, g1, g3, k):
    x1, y1, z1, x2, y2, z2 = xyzxyz

    dx1 = a * (y1 - x1 - g(x1, g1, g3))
    dy1 = x1 - y1 + z1 + k * (y2 - y1)
    dz1 = -b * y1

    dx2 = a * (y2 - x2 - g(x2, g1, g3))
    dy2 = x2 - y2 + z2 + k * (y1 - y2)
    dz2 = -b * y2

    return [dx1, dy1, dz1, dx2, dy2, dz2]

############## Try uncommenting / modifying the parameters below

# Double Scroll
params, xyz0 = (78/5, 28, -8/7, -5/7), [0.7, 0, 0]

# A different Double Scroll
# params, xyz0 = (-4.087, -2, -8/7, -5/7), [1, 0, 0]

# Single Scroll
# params, xyz0 = (-6.691, -1.52, -8/7, -5/7), [1, 0, 0]

# A different Single Scroll
# params, xyz0 = (7.9521, 11.925, -1.146, -0.8533), [0.1, 0, 0]

#################################

sns.set()

# Plot phase space of simple Chua circuit
r = solve_ivp(y_prime_simple,
              y0=xyz0,
              t_span=(0, 100),
              t_eval=np.linspace(0, 100, num=5000),
              args=params)

t = r.t
x, y, z = r.y

sns.lineplot(t, x, label='$X = V_{C_1}$', linewidth=1)
sns.lineplot(t, y, label='$Y = V_{C_2}$', linewidth=1)
sns.lineplot(t, z, label='$Z = I_L$', linewidth=1)

plt.gca().set_xlabel('time')
plt.legend()
plt.title('Voltages and currents of the simple Chua circuit')
plt.tight_layout()
plt.savefig('chua_simple_time.pdf')
plt.close()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(xs=x, ys=y, zs=z)
plt.title('$V_{C_1}$ vs. $V_{C_2}$ vs. $I_L$ in the simple Chua circuit')
plt.tight_layout()
plt.savefig('chua_simple_phase.pdf')
plt.close()

######### Synchronization in two coupled Chua circuits

# The last param, `0.8` is the coupling coefficient (or strength).
# Try adjusting it as well.
# The two rows of `xyzxyz0` are the initial conditions for the two circuits.
#
# Try adjusting the parameters until you achieve synchronization.
# Search the Internet for more interesting settings of the parameters.
# How can you tell when synchronization happens?
# Hint: look at the one-vs-the-other voltage (and current) plots below!
params, xyzxyz0 = (78/5, 28, -8/7, -5/7, 0.8), [[0.7, 0, 0],
                                                [-0.5, 0, 0.1]]

r = solve_ivp(y_prime_coupled,
              y0=np.ravel(xyzxyz0),
              t_span=(0, 100),
              t_eval=np.linspace(0, 100, num=5000),
              args=params)

t = r.t
x1, y1, z1, x2, y2, z2 = r.y


sns.lineplot(t, x1, label='$X1 = V_{C_{1,1}}$', linewidth=1)
sns.lineplot(t, y1, label='$Y1 = V_{C_{1,2}}$', linewidth=1)
sns.lineplot(t, z1, label='$Z1 = I_{L_1}$', linewidth=1)
sns.lineplot(t, x2, label='$X2 = V_{C_{2,1}}$', linewidth=1)
sns.lineplot(t, y2, label='$Y2 = V_{C_{2,2}}$', linewidth=1)
sns.lineplot(t, z2, label='$Z2 = I_{L_2}$', linewidth=1)

plt.gca().set_xlabel('time')
plt.legend()
plt.title('Voltages and currents of two coupled Chua circuits')
plt.tight_layout()
plt.savefig('chua_coupled_time.pdf')
plt.close()

########## Try plotting 1 or 2 or all 3 of these:
sns.lineplot(x1, x2, sort=False, label='$V_{C_{2,1}}$ vs. $V_{C_{1,1}}$')
# sns.lineplot(y1, y2, sort=False, label='$V_{C_{2,2}}$ vs. $V_{C_{1,2}}$')
# sns.lineplot(z1, z2, sort=False, label='$I_{L_2}}$ vs. $I_{L_1}$')

plt.legend()
plt.title('Comparison of two coupled Chua circuits')
plt.tight_layout()
plt.savefig('chua_coupled_phase.pdf')
plt.close()
