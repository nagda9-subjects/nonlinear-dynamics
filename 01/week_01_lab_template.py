# Import numeric and plotting packages
import numpy as np
import matplotlib as mpl; mpl.use('tkagg')
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.integrate import solve_ivp

### You only have to modify code from this comment ###

def y_prime(t, y, b):
    # Inputs: [y[0], y[1]] = [y, y']
    # Returns: [y', y'']  = [???, ???]
    return ???, ???

t_span = (0, 4 * np.pi)                # Time span to integrate over
y0 = [1, 0]                            # Initial values
t_eval = np.linspace(*t_span, num=256) # Return solutions at these `t` points
b = 0.5                                # Damping parameter

### You only have to modify code until this comment ###

# Actually integrate the DE
solution = solve_ivp(y_prime, t_span, y0, t_eval=t_eval, args=(b,))
t, y = solution.t, solution.y

fig = plt.figure(figsize=plt.figaspect(1/3))
ax1 = fig.add_subplot(1, 2, 1) # 2D subplot
# ax2 = fig.add_subplot(1, 2, 2, projection='3d')
ax2 = fig.add_subplot(1, 2, 2)

# Plot y and y'
sns.lineplot(t, y[0], color='red',  label='y',     ax=ax1)
sns.lineplot(t, y[1], color='blue', label='dy/dt', ax=ax1)
ax1.set_ylim(-1, 1)
ax1.legend(loc='upper right')
ax1.set_title('Damped Pendulum: Position and Momentum')

# Plot energy levels
X, Y = ???
Z = ???
levels = np.linspace(0, np.sqrt(8), num=10)**2
c = ax2.contour(X, Y, Z, levels=levels, cmap='RdBu_r')
plt.colorbar(c)
ax2.set_title('Damped Pendulum: Energy Levels')

plt.savefig('week_01_lab.pdf')
